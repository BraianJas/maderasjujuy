import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    console.log('getparagraphtext')
    return element(by.deepCss('app-root ion-content')).getText();
  }
}
