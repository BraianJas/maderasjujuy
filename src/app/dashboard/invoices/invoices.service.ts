import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import { FormControl } from "@angular/forms";
import { environment } from "../../../environments/environment";

const API_AUTH_URL = `${environment.API_PATH}auth/`;
const API_REFLECT_URL = `${environment.API_PATH}`;

@Injectable({
  providedIn: "root"
})
export class InvoicesService {
  baseURL: String;
  headers: any;

  constructor(public http: HttpClient, private storage: Storage) {}

  set_headers() {
    return new Promise(resolve => {
      this.storage.get("jwt").then(val => {
        this.headers = new HttpHeaders({
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: "Bearer " + val
        });
        resolve();
      });
      console.log(this.storage);
    });

    
  }

  close: any;
  closeSession() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(API_AUTH_URL + `logout`, {}, { headers: this.headers })
          .subscribe(data => {
            this.storage.remove("maderera-dash-jwt");
            this.close = data;
            resolve(this.close);
          });
      });
    });
  }

  invoice: any;
  resultInvoice: any;
  profileImg: any;
  getInvoice() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "invoice", { headers: this.headers })
          .subscribe(data => {
            this.resultInvoice = data;
            if (this.resultInvoice.length > 0) {
              this.invoice = this.resultInvoice[0];
              /*this.getProfileImg().then(() => {
                this.user.pic = this.profileImg[0].pic;
                resolve(this.user);
              });*/
              this.invoice.pic = "";
              resolve(this.invoice);
            } else {
              this.closeSession().then(() => {
                resolve(this.resultInvoice);
              });
            }
          });
      });
    });
  }

  // -------------------------- Local DB ------------------------- //
  facturas: any;
  getFacturas(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        //this.http.get(this.baseURL + `reflect/user/user-group/group?filters=a.active:1,a.last_name LIKE ${param.user}&offset=${param.offset}&limit=${param.limit}`, { headers: this.headers })
        this.http
          .get(API_REFLECT_URL + `facturas/facturas-group/group`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.facturas = data;
            if (
              typeof this.facturas.status !== "undefined" &&
              this.facturas.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.facturas);
              });
            } else {
              resolve(this.facturas);
            }
          });
      });
    });
  }

  /*groups: any;
  getGroups() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
        .get(`${API_REFLECT_URL}getallgroups.php`,
          {
            headers: this.headers
          }
        )
          .subscribe(data => {
            this.groups = data;
            if (
              typeof this.groups.status !== "undefined" &&
              this.groups.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.groups);
              });
            } else {
              resolve(this.groups);
            }
          });
      });
    });
  }*/

  getInvoices() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REFLECT_URL + `invoice`, { headers: this.headers })
          .subscribe(data => {
            this.facturas = data;
            if (
              typeof this.facturas.status !== "undefined" &&
              this.facturas.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.facturas);
              });
            } else {
              resolve(this.facturas);
            }
          });
      });
    });
  }

  //acá hace la llamada al grid de user-list
  /*getProvidersGroup(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        //this.http.get(this.baseURL + `reflect/user/user-group/group?filters=a.active:1,first_name last_name dni LIKE ${param.user}&offset=${param.offset}&limit=${param.limit}`, { headers: this.headers })
        this.http
          .get(`${API_REFLECT_URL}getallproviders.php`,
            {
              headers: this.headers
            }
          )
          .subscribe(data => {
            this.proveedores = data;
            if (
              typeof this.proveedores.status !== "undefined" &&
              this.proveedores.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.proveedores);
              });
            } else {
              resolve(this.proveedores);
            }
          });
      });
    });
  }*/

  getAInvoice(invoiceId: number) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REFLECT_URL + `invoice?filters=a.id:${invoiceId}`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.invoice = data;
            if (
              typeof this.invoice.status !== "undefined" &&
              this.invoice.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.invoice);
              });
            } else {
              resolve(this.invoice);
            }
          });
      });
    });
  }

  /*permissions: any;
  getPermission() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "permissions", { headers: this.headers })
          .subscribe(data => {
            this.permissions = data;
            if (
              typeof this.permissions.status !== "undefined" &&
              this.permissions.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.permissions);
              });
            } else {
              resolve(this.permissions);
            }
          });
      });
    });
  }*/

  addInvoice(invoice: any): any {

    let url = API_REFLECT_URL + "create_invoice.php";

    let dataInvoice = JSON.stringify(invoice);
    console.log(invoice);
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, dataInvoice, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            /* let par = {
                "user_id": response.insertId,
                "newPassword": user.password
              };
              this.changeAPassword(par)
                  .subscribe((data) => {
                    console.log(data);
                      if (data.status) {
                          this.user.password = data.password;
                      }
                  }); */
            resolve(response);
          }
        });
      });
    });
  }

  deleteInvoice(invoice: any): any {
    let url = API_REFLECT_URL + "delete_invoice.php";
    console.log(invoice);
    invoice.active = 0;
    let data = invoice
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, data, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            resolve(response);
          }
        });
      });
    });
  }

  updateInvoice(invoice: any) {
    let url = API_REFLECT_URL + "update_invoice.php";
    let data = JSON.stringify(invoice);
    let response: any;
    console.log(invoice);
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(url, data, { headers: this.headers })
          .subscribe(data => {
            response = data;
            if (
              typeof response.status !== "undefined" &&
              response.status == false
            ) {
              this.closeSession().then(() => {
                resolve(response);
              });
            } else {
              resolve(response);
            }
          });
      });
    });
  }
}


