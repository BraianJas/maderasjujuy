import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { InvoicesPage } from "./invoices-list/invoices-list.page";

import { ImageCropperModule } from "ngx-image-cropper";

const routes: Routes = [
  {
    path: "",
    component: InvoicesPage
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ImageCropperModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    InvoicesPage,
  ],
  entryComponents: []
})
export class InvoicesPageModule {}

