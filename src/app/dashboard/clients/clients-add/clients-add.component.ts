import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import {
  FormArray,
  FormBuilder,
  FormGroup,
  FormControl,
  ReactiveFormsModule,
  Validators
} from "@angular/forms";
import { NavParams, LoadingController, ToastController } from "@ionic/angular";
import { ClientsService } from "../clients.service";

/* import { ApiServiceProvider } from '../../providers/api-service/api-service'; */
import { LoginPage } from "../../../authentication/login/login.page";
import { ClientsPage } from "../clients-list/clients.page";

/**
 * Generated class for the UsersAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'app-clients-add',
  templateUrl: './clients-add.component.html',
  styleUrls: ['./clients-add.component.scss'],
})
export class ClientsAddComponent {
  @Input() clientPar: any;
  @Input() idx: any;
  @Input() onSave: any;
  @Input() onCancel: any;

  loader: any;
  dataStatus: any;
  hide_sm: any;
  isBrowser: Boolean = false;
  messaje: string = "";
  class: string = "alert d-none";
  client: any = {
    id: null,
    apynom: "",
    landline: "",
    phone: "",
    dni: "",
    purchase_margin: "",
    iva_category: "",
    address: "",
    client_typeId: null,
    discount: ""
  };
  clientForm: FormGroup;
  validation_messages: any;

  constructor(
    private fb: FormBuilder,
    private service: ClientsService,
    private navParams: NavParams,
    public loadingController: LoadingController,
    public toastController: ToastController
  ) {
    /*     if (this.platform.is('mobileweb') || this.platform.is('core')) {
          this.isBrowser = true;
      }
      this.hide_sm = platform.width() < 576; */
    this.client = this.navParams.get("clientPar");
    this.onSave = this.navParams.get("onSave");
    this.onCancel = this.navParams.get("onCancel");
    this.idx = this.navParams.get("idx");
    this.createForm();
  }

  /* ionViewDidLoad() {
      this.getGroups();
    } */

  /* clear() {
      this.user = {
        id: 0,
        dni: 0,
        last_name: "",
        first_name: "",
        phone: "",
        email: "",
        password: "",
        active: 1,
        is_admin: 1,
        sys_group_id : 1
    };
    this.messaje = "";
    this.class= 'alert d-none';
    this.userForm.reset({
        id: 0,
        dni: 0,
        last_name: "",
        first_name: "",
        phone: "",
        email: "",
        password: "",
        active: 1,
        is_admin: 1,
        sys_group_id: 1
    });
  } */

  createForm() {
    this.validation_messages = {
      apynom: [
        {
          type: "minlength",
          message: "El nombre debe tener mas de 3 caracteres."
        },
        { type: "required", message: "El nombre es obligatorio." }
      ],
      landline: [
        { type: "required", message: "El TELEFONO es obligatorio." },
        {
          type: "pattern",
          message:
            "El TELEFONO debe contener solo numeros y su longitud debe ser entre 7 y 9 digitos."
        }
      ],
      phone: [
        { type: "required", message: "El CELULAR es obligatorio." },
        {
          type: "pattern",
          message:
            "El CELULAR debe contener solo numeros y su longitud debe ser entre 7 y 9 digitos."
        }
      ],
      dni: [
        {
          type: "required",
          message: "El DNI debe tener mas de 3 caracteres."
        },
        { type: "required", message: "El dni es obligatorio." }
      ],
      purchase_margin: [
        {
          type: "required",
          message: "El MARGEN DE COMPRA debe tener mas de 3 caracteres."
        },
        { type: "required", message: "El margen de compra es obligatorio." }
      ],
      iva_category: [
        {
          type: "pattern",
          message: "La categoria de iva no coincide con un formato válido."
        },
        { type: "required", message: "La categoria de iva email es obligatorio." },
      ],
      address: [
        {
          type: "pattern",
          message: "La direccion no coincide con un formato válido."
        },
        { type: "required", message: "La direccion es obligatorio." },
      ],
      client_typeId: [
        {
          type: "pattern",
          message: "El tipo de cliente no coincide con un formato válido."
        },
        { type: "required", message: "El tipo de cliente es obligatorio." },
      ],
      discount: [
        {
          type: "pattern",
          message: "El descuento no coincide con un formato válido."
        },
        { type: "required", message: "El descuento es obligatorio." },
      ],
      
    };
    this.clientForm = this.fb.group({
      id: this.client.id,
      apynom: new FormControl(
        this.client.apynom,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),
      landline: new FormControl(
        this.client.landline,
        Validators.compose([
          Validators.required,
          Validators.pattern("^\\d{7,15}$")
        ])
      ),
      phone: new FormControl(
        this.client.phone,
        Validators.compose([
          Validators.required,
          Validators.pattern("^\\d{7,15}$")
        ])
      ),
      dni: new FormControl(
        this.client.dni,
        Validators.compose([
          Validators.required,
          Validators.pattern("^\\d{7,9}$")
        ])
      ),
      purchase_margin: new FormControl(
        this.client.purchase_margin,
        Validators.compose([
          Validators.required,
          Validators.pattern("^\\d{1,9}$")
        ])
      ),      
      address: new FormControl(
        this.client.address,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),
      client_type: new FormControl(
        this.client.client_type,
        Validators.compose([Validators.required, Validators.minLength(3)])
      ),
      discount: new FormControl(
        this.client.discount,
        Validators.compose([Validators.required, Validators.pattern("^\\d{0,3}$")])
      ),
      
    });
  }

  save() {
    //this.presentToast("Guardandoo...", "alert alert-secondary");
    //this.user.id = null;
    //this.validate_sys_user((response) => response.JSON());
    let client = this.clientForm.value;

    if (typeof client.id !== "undefined" && client.id !== null) {
      this.service
        .updateClient(client)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            //Login
            this.presentToast("No se pudo actualizar", "danger");
          } else {
            this.presentToast("Cliente Actualizado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      this.service
        .addClient(client)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            console.log("Status FALSE", data);
            this.presentToast("No se pudo crear el cliente", "danger");
          } else {
            client.id = this.dataStatus.insertId;
            this.presentToast("Cliente Creado", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    this.onSave(client, this.idx);
  }

  async presentToast(_messaje, _color) {
    let toast = await this.toastController.create({
      message: _messaje,
      duration: 3000,
      color: _color,
      //showCloseButton: false,
      position: "bottom",
      //closeButtonText: "X",
      animated: true,
      keyboardClose: true,
      translucent: true
    });

    toast.present();
  }

  cancel() {
    //this.navCtrl.setRoot(UsuariosPage, {}, { animate: true, direction: 'forward' });
    //this.router.navigate(['dashboard/users']);
    this.onCancel();
  }
}

