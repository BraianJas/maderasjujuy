import { TestBed } from '@angular/core/testing';

import { CashregistersService } from './cashregisters.service';

describe('CashregistersService', () => {
  let service: CashregistersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CashregistersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
