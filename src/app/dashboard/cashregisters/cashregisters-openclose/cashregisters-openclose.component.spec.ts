import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CashregistersOpencloseComponent } from './cashregisters-openclose.component';

describe('CashregistersOpencloseComponent', () => {
  let component: CashregistersOpencloseComponent;
  let fixture: ComponentFixture<CashregistersOpencloseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashregistersOpencloseComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CashregistersOpencloseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
