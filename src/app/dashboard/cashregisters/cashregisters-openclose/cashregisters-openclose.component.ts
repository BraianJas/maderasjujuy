import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import {
  FormArray,
  FormBuilder,
  FormGroup,
  FormControl,
  ReactiveFormsModule,
  Validators
} from "@angular/forms";
import { NavParams, LoadingController, ToastController } from "@ionic/angular";
import { CashregistersService } from "../cashregisters.service";

import { CashregistersPage } from "../cashregisters-list/cashregisters-list.page";

/**
 * Generated class for the UsersAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'app-cashregisters-openclose',
  templateUrl: './cashregisters-openclose.component.html',
  styleUrls: ['./cashregisters-openclose.component.scss'],
})
export class CashregistersOpencloseComponent {
  @Input() cashregisterPar: any;
  @Input() idx: any;
  @Input() onSave: any;
  @Input() onCancel: any;

  loader: any;
  dataStatus: any;
  hide_sm: any;
  isBrowser: Boolean = false;
  messaje: string = "";
  class: string = "alert d-none";
  cashregister: any = {
    id: null,
    date_start: "",
    date_end: "",
    number: "",
    open_amount: "",
    close_amount: "",
    userid: 1,
    active: "",
  };
  cashregisterForm: FormGroup;
  validation_messages: any;

  constructor(
    private fb: FormBuilder,
    private service: CashregistersService,
    private navParams: NavParams,
    public loadingController: LoadingController,
    public toastController: ToastController
  ) {
    /*     if (this.platform.is('mobileweb') || this.platform.is('core')) {
          this.isBrowser = true;
      }
      this.hide_sm = platform.width() < 576; */
    this.cashregister = this.navParams.get("cashregisterPar");
    this.onSave = this.navParams.get("onSave");
    this.onCancel = this.navParams.get("onCancel");
    this.idx = this.navParams.get("idx");
    this.createForm();
  }

  /* ionViewDidLoad() {
      this.getGroups();
    } */

  /* clear() {
      this.user = {
        id: 0,
        dni: 0,
        last_name: "",
        first_name: "",
        phone: "",
        email: "",
        password: "",
        active: 1,
        is_admin: 1,
        sys_group_id : 1
    };
    this.messaje = "";
    this.class= 'alert d-none';
    this.userForm.reset({
        id: 0,
        dni: 0,
        last_name: "",
        first_name: "",
        phone: "",
        email: "",
        password: "",
        active: 1,
        is_admin: 1,
        sys_group_id: 1
    });
  } */

  createForm() {
    this.validation_messages = {
      date_start: [
        {
          type: "date",
        },
      ],
      date_end: [
        {
          type: "date",
        },
      ],
      number: [
        { 
          type: "required", 
        },
      ],
      open_amount: [
        { 
          type: "required", 
        },
      ],
      close_amount: [
        { 
          type: "required", 
        },
      ],
      
    };
    this.cashregisterForm = this.fb.group({
      id: this.cashregister.id,
      date_start: new FormControl(
        this.cashregister.date_start,
        Validators.compose([Validators.required])
      ),
      date_end: new FormControl(
        this.cashregister.date_end,
        Validators.compose([
          Validators.required,
        ])
      ),
      number: new FormControl(
        this.cashregister.number,
        Validators.compose([
          Validators.required,
        ])
      ),
      open_amount: new FormControl(
        this.cashregister.open_amount,
        Validators.compose([
          Validators.required,
        ])
      ),      
      close_amount: new FormControl(
        this.cashregister.close_amount,
        Validators.compose([Validators.required])
      ),
      
    });
  }

  save() {
    //this.presentToast("Guardandoo...", "alert alert-secondary");
    //this.user.id = null;
    //this.validate_sys_user((response) => response.JSON());
    let cashregister = this.cashregisterForm.value;

    if (typeof cashregister.id !== "undefined" && cashregister.id !== null) {
      this.service
        .updateCashregister(cashregister)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            //Login
            this.presentToast("No se pudo actualizar", "danger");
          } else {
            this.presentToast("Caja Actualizada", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      this.service
        .addCashregister(cashregister)
        .then(data => {
          this.dataStatus = data;
          if (
            typeof this.dataStatus.status !== "undefined" &&
            this.dataStatus.status == false
          ) {
            console.log("Status FALSE", data);
            this.presentToast("No se pudo crear la Caja", "danger");
          } else {
            cashregister.id = this.dataStatus.insertId;
            this.presentToast("Caja Creada", "success");
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    this.onSave(cashregister, this.idx);
  }

  async presentToast(_messaje, _color) {
    let toast = await this.toastController.create({
      message: _messaje,
      duration: 3000,
      color: _color,
      //showCloseButton: false,
      position: "bottom",
      //closeButtonText: "X",
      animated: true,
      keyboardClose: true,
      translucent: true
    });

    toast.present();
  }

  cancel() {
    //this.navCtrl.setRoot(UsuariosPage, {}, { animate: true, direction: 'forward' });
    //this.router.navigate(['dashboard/users']);
    this.onCancel();
  }
}


