import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import { FormControl } from "@angular/forms";
import { environment } from "../../../environments/environment";

const API_AUTH_URL = `${environment.API_PATH}auth/`;
const API_REFLECT_URL = `${environment.API_PATH}`;

@Injectable({
  providedIn: 'root'
})
export class CashregistersService {
  baseURL: String;
  headers: any;

  constructor(public http: HttpClient, private storage: Storage) { }

  set_headers() {
    return new Promise(resolve => {
      this.storage.get("jwt").then(val => {
        this.headers = new HttpHeaders({
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: "Bearer " + val
        });
        resolve();
      });
      console.log(this.storage);
    });    
  }

  close: any;
  closeSession() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(API_AUTH_URL + `logout`, {}, { headers: this.headers })
          .subscribe(data => {
            this.storage.remove("maderera-dash-jwt");
            this.close = data;
            resolve(this.close);
          });
      });
    });
  }

  cashregister: any;
  resultCashregister: any;
  getCashregister() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "cashregister", { headers: this.headers })
          .subscribe(data => {
            this.resultCashregister = data;
            if (this.resultCashregister.length > 0) {
              this.cashregister = this.resultCashregister[0];
              this.cashregister.pic = "";
              resolve(this.cashregister);
            } else {
              this.closeSession().then(() => {
                resolve(this.resultCashregister);
              });
            }
          });
      });
    });
  }

  // Faltan servicios de img

  // -------------------------- Local DB ------------------------- //
  cajas: any;
  getCajas(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        //this.http.get(this.baseURL + `reflect/user/user-group/group?filters=a.active:1,a.last_name LIKE ${param.user}&offset=${param.offset}&limit=${param.limit}`, { headers: this.headers })
        this.http
          .get(API_REFLECT_URL + `cashregister/cashregister-group/group`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.cajas = data;
            if (
              typeof this.cajas.status !== "undefined" &&
              this.cajas.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.cajas);
              });
            } else {
              resolve(this.cajas);
            }
          });
      });
    });
  }

  getCashregisters() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REFLECT_URL + `cashregister`, { headers: this.headers })
          .subscribe(data => {
            this.cajas = data;
            if (
              typeof this.cajas.status !== "undefined" &&
              this.cajas.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.cajas);
              });
            } else {
              resolve(this.cajas);
            }
          });
      });
    });
  }

  //acá hace la llamada al grid de user-list
  getCashregistersGroup(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        //this.http.get(this.baseURL + `reflect/user/user-group/group?filters=a.active:1,first_name last_name dni LIKE ${param.user}&offset=${param.offset}&limit=${param.limit}`, { headers: this.headers })
        this.http
          .get(`${API_REFLECT_URL}getallcashregisters.php`,
            {
              headers: this.headers
            }
          )
          .subscribe(data => {
            this.cajas = data;
            if (
              typeof this.cajas.status !== "undefined" &&
              this.cajas.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.cajas);
              });
            } else {
              resolve(this.cajas);
            }
          });
      });
    });
  }

  getACashregister(cashregisterId: number) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REFLECT_URL + `cashregister?filters=a.id:${cashregisterId}`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.cashregister = data;
            if (
              typeof this.cashregister.status !== "undefined" &&
              this.cashregister.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.cashregister);
              });
            } else {
              resolve(this.cashregister);
            }
          });
      });
    });
  }

  /*permissions: any;
  getPermission() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "permissions", { headers: this.headers })
          .subscribe(data => {
            this.permissions = data;
            if (
              typeof this.permissions.status !== "undefined" &&
              this.permissions.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.permissions);
              });
            } else {
              resolve(this.permissions);
            }
          });
      });
    });
  }*/

  addCashregister(user: any): any {
    let url = API_REFLECT_URL + "create_cashregister.php";
    let data = JSON.stringify(user);
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, data, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            /* let par = {
                "user_id": response.insertId,
                "newPassword": user.password
              };
              this.changeAPassword(par)
                  .subscribe((data) => {
                    console.log(data);
                      if (data.status) {
                          this.user.password = data.password;
                      }
                  }); */
            resolve(response);
          }
        });
      });
    });
  }

  /*   deleteUser(user: any): any {
    let url = this.baseURL + 'reflect/user/';
    let data = '{"id": '+user.id+'}';
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    return this.http.delete(url, httpOptions);
  } */

  deleteCashregister(cashregister: any): any {
    let url = API_REFLECT_URL + "delete_cashregister.php";
    console.log(cashregister);
    cashregister.active = 0;
    let data = cashregister
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, data, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            resolve(response);
          }
        });
      });
    });
  }

  updateCashregister(cashregister: any) {
    let url = API_REFLECT_URL + "update_cashregister.php";
    let data = JSON.stringify(cashregister);
    let response: any;
    console.log(cashregister);
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(url, data, { headers: this.headers })
          .subscribe(data => {
            response = data;
            if (
              typeof response.status !== "undefined" &&
              response.status == false
            ) {
              this.closeSession().then(() => {
                resolve(response);
              });
            } else {
              resolve(response);
            }
          });
      });
    });
  }
}
