import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CashregistersListPage } from './cashregisters-list.page';

describe('CashregistersListPage', () => {
  let component: CashregistersListPage;
  let fixture: ComponentFixture<CashregistersListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashregistersListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CashregistersListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
