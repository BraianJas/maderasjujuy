import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { CashregistersPage } from "./cashregisters-list/cashregisters-list.page";
import { CashregistersOpencloseComponent } from "./cashregisters-openclose/cashregisters-openclose.component";
import { ImageCropperModule } from "ngx-image-cropper";
//import { DriversComponent } from "./users-drivers/drivers/drivers.component";

const routes: Routes = [
  {
    path: "",
    component: CashregistersPage
  },
  {
    path: "openclose",
    component: CashregistersOpencloseComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ImageCropperModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    CashregistersPage,
    CashregistersOpencloseComponent,
  ],
  entryComponents: []
})
export class CashregistersModule { }
