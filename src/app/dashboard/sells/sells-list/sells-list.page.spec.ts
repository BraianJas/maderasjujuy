import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SellsListPage } from './sells-list.page';

describe('SellsListPage', () => {
  let component: SellsListPage;
  let fixture: ComponentFixture<SellsListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellsListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SellsListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
