import { Component, OnInit, ViewChild } from "@angular/core";
import { ModalController, AlertController } from "@ionic/angular";
import { DetailinvoicesService } from "../detailinvoices.service";
import { ExcelServiceProvider } from "../../../services/excelService/excel-service.service";
import { ScreenService } from "../../../app-screen.service";

@Component({
  selector: 'app-detailinvoices-list',
  templateUrl: './detailinvoices-list.page.html',
  styleUrls: ['./detailinvoices-list.page.scss'],
})
export class DetailinvoicesPage implements OnInit {
  @ViewChild("inputSearch", { static: true }) inputSearch;

  detailinvoices: any;
  detailinvoicesSinFiltro: any;
  isLoading = false;
  searchDetailInvoice: Object = { detailinvoices: "", offset: 0, limit: 200 };
  global_vars: any;
  infiniteRow: Number = 100;
  hide_sm: any;
  detailinvoice: string = "";
  dataStatus: any;
  editModal: any;
  screen: any;

  constructor(
    private service: DetailinvoicesService,
    public modalController: ModalController,
    public alertController: AlertController,
    public excelService: ExcelServiceProvider,
    private screenService: ScreenService
  ) {
    screenService.screen.subscribe(_screen => {
      this.screen = _screen;
    });

    this.getDetailInvoices();
    
    this.hide_sm = false;
  }

  ngOnInit() {
    //this.getUsers();
  }

  getDetailInvoices() {
    this.searchDetailInvoice["detailinvoice"] = this.detailinvoice;
    this.searchDetailInvoice["offset"] = 0;
    this.searchDetailInvoice["limit"] = this.infiniteRow;
    this.isLoading = true;
    /*this.service                                REVISAR
      .getProvidersGroup(this.searchProvider)
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
          //this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'forward' });
        } else {
          this.providers = this.dataStatus.providers;
          console.log(this.providers);
          this.providersSinFiltro = this.dataStatus;
          //console.log(this.users);
          this.inputSearch.setFocus();
        }
      })
      .catch(err => {
        console.log(err);
      });*/
    this.isLoading = false;
  }

  /*async edit(providerIndex: any) {
    let provider = this.providers[providerIndex];
    this.editModal = await this.modalController.create({
      component: ProvidersAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        providerPar: provider,
        idx: providerIndex,
        onSave: this.saveProviderEdited,
        onCancel: this.cancelProviderEdition
      }
    });
    return await this.editModal.present();
  }

  async addProvider() {
    let providerPar = {
      id: null,
      cuit: "",
      company_name: "",
      ivacategid: "",
      phone: "",
      active: 1
    };
    this.editModal = await this.modalController.create({
      component: ProvidersAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        providerPar: providerPar,
        idx: null,
        onSave: this.addNewProvider,
        onCancel: this.cancelProviderEdition
      }
    });
    return await this.editModal.present();
  }*/

  downloadExcel() {
    this.excelService.exportAsExcelFile(this.detailinvoices, "detailinvoices");
  }

  /*saveProviderEdited = (providerInfo: any, providerIndex: any) => {
    this.providers[providerIndex] = providerInfo;
    this.editModal.dismiss();
  };

  addNewProvider = (providerInfo: any, providerIndex: any) => {
    this.providers.push(providerInfo);
    this.editModal.dismiss();
  };

  cancelProviderEdition = () => {
    this.editModal.dismiss();
  };*/

  async delete(detailinvoiceIndex: any) {
    const alert = await this.alertController.create({
      header: "Quitar Detalle Factura",
      message: "¿Confirma que quiere borrar el detalle de factura?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "danger",
          handler: blah => {
            console.log("Confirm Cancel: blah");
          }
        },
        {
          text: "Si",
          handler: () => {
            let detailinvoice = this.detailinvoices[detailinvoiceIndex];
            let par = { id: detailinvoice.id, active: 0 };

            this.service
              .deleteDetailInvoice(par)
              .then(data => {
                this.dataStatus = data;
                if (
                  typeof this.dataStatus.status !== "undefined" &&
                  this.dataStatus.status == false
                ) {
                  console.log("Login");
                } else {
                  this.getDetailInvoices();
                }
              })
              .catch(err => {
                console.log(err);
              });
          }
        }
      ]
    });

    await alert.present();
  }

  initializeItems() {
    this.detailinvoices = this.detailinvoicesSinFiltro;
  }

  getItems(ev: any) {
    console.log(ev);

    /* if (ev.cancelable) {
      this.getUsers();
    } */

    // Reset items back to all of the items
    //this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != "") {
      this.detailinvoices = this.detailinvoices.filter(item => {
        return (
          JSON.stringify(item)
            .toLowerCase()
            .indexOf(val.toLowerCase()) > -1
        );
      });

      if (this.detailinvoice.length == 0) {
        this.getDetailInvoices();
      }
    }
  }
}



