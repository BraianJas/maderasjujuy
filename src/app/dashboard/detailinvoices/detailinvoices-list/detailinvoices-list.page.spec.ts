import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailinvoicesListPage } from './detailinvoices-list.page';

describe('DetailinvoicesListPage', () => {
  let component: DetailinvoicesListPage;
  let fixture: ComponentFixture<DetailinvoicesListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailinvoicesListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailinvoicesListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
