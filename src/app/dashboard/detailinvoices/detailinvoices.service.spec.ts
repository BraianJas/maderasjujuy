import { TestBed } from '@angular/core/testing';

import { DetailinvoicesService } from './detailinvoices.service';

describe('DetailinvoicesService', () => {
  let service: DetailinvoicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DetailinvoicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
