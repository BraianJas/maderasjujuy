import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import { FormControl } from "@angular/forms";
import { environment } from "../../../environments/environment";

const API_AUTH_URL = `${environment.API_PATH}auth/`;
const API_REFLECT_URL = `${environment.API_PATH}`;

@Injectable({
  providedIn: "root"
})
export class DetailinvoicesService {
  baseURL: String;
  headers: any;

  constructor(public http: HttpClient, private storage: Storage) {}

  set_headers() {
    return new Promise(resolve => {
      this.storage.get("jwt").then(val => {
        this.headers = new HttpHeaders({
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: "Bearer " + val
        });
        resolve();
      });
      console.log(this.storage);
    });

    
  }

  close: any;
  closeSession() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(API_AUTH_URL + `logout`, {}, { headers: this.headers })
          .subscribe(data => {
            this.storage.remove("maderera-dash-jwt");
            this.close = data;
            resolve(this.close);
          });
      });
    });
  }

  detailinvoice: any;
  resultDetailinvoice: any;
  profileImg: any;
  getDetailinvoice() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "detailinvoice", { headers: this.headers })
          .subscribe(data => {
            this.resultDetailinvoice = data;
            if (this.resultDetailinvoice.length > 0) {
              this.detailinvoice = this.resultDetailinvoice[0];
              /*this.getProfileImg().then(() => {
                this.user.pic = this.profileImg[0].pic;
                resolve(this.user);
              });*/
              this.detailinvoice.pic = "";
              resolve(this.detailinvoice);
            } else {
              this.closeSession().then(() => {
                resolve(this.resultDetailinvoice);
              });
            }
          });
      });
    });
  }

  // -------------------------- Local DB ------------------------- //
  detallefacturas: any;
  getDetalleFacturas(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        //this.http.get(this.baseURL + `reflect/user/user-group/group?filters=a.active:1,a.last_name LIKE ${param.user}&offset=${param.offset}&limit=${param.limit}`, { headers: this.headers })
        this.http
          .get(API_REFLECT_URL + `detallefacturas/detallefacturas-group/group`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.detallefacturas = data;
            if (
              typeof this.detallefacturas.status !== "undefined" &&
              this.detallefacturas.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.detallefacturas);
              });
            } else {
              resolve(this.detallefacturas);
            }
          });
      });
    });
  }

  /*groups: any;
  getGroups() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
        .get(`${API_REFLECT_URL}getallgroups.php`,
          {
            headers: this.headers
          }
        )
          .subscribe(data => {
            this.groups = data;
            if (
              typeof this.groups.status !== "undefined" &&
              this.groups.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.groups);
              });
            } else {
              resolve(this.groups);
            }
          });
      });
    });
  }*/

  getDetailInvoices() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REFLECT_URL + `detailinvoice`, { headers: this.headers })
          .subscribe(data => {
            this.detallefacturas = data;
            if (
              typeof this.detallefacturas.status !== "undefined" &&
              this.detallefacturas.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.detallefacturas);
              });
            } else {
              resolve(this.detallefacturas);
            }
          });
      });
    });
  }

  //acá hace la llamada al grid de user-list
  /*getProvidersGroup(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        //this.http.get(this.baseURL + `reflect/user/user-group/group?filters=a.active:1,first_name last_name dni LIKE ${param.user}&offset=${param.offset}&limit=${param.limit}`, { headers: this.headers })
        this.http
          .get(`${API_REFLECT_URL}getallproviders.php`,
            {
              headers: this.headers
            }
          )
          .subscribe(data => {
            this.proveedores = data;
            if (
              typeof this.proveedores.status !== "undefined" &&
              this.proveedores.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.proveedores);
              });
            } else {
              resolve(this.proveedores);
            }
          });
      });
    });
  }*/

  getADetailinvoice(detailinvoiceId: number) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REFLECT_URL + `detailinvoice?filters=a.id:${detailinvoiceId}`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.detailinvoice = data;
            if (
              typeof this.detailinvoice.status !== "undefined" &&
              this.detailinvoice.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.detailinvoice);
              });
            } else {
              resolve(this.detailinvoice);
            }
          });
      });
    });
  }

  /*permissions: any;
  getPermission() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "permissions", { headers: this.headers })
          .subscribe(data => {
            this.permissions = data;
            if (
              typeof this.permissions.status !== "undefined" &&
              this.permissions.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.permissions);
              });
            } else {
              resolve(this.permissions);
            }
          });
      });
    });
  }*/

  addDetailinvoice(detailinvoice: any): any {

    let url = API_REFLECT_URL + "create_detailinvoice.php";

    let dataDetailinvoice = JSON.stringify(detailinvoice);
    console.log(detailinvoice);
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, dataDetailinvoice, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            /* let par = {
                "user_id": response.insertId,
                "newPassword": user.password
              };
              this.changeAPassword(par)
                  .subscribe((data) => {
                    console.log(data);
                      if (data.status) {
                          this.user.password = data.password;
                      }
                  }); */
            resolve(response);
          }
        });
      });
    });
  }

  deleteDetailInvoice(detailinvoice: any): any {
    let url = API_REFLECT_URL + "delete_detailinvoice.php";
    console.log(detailinvoice);
    detailinvoice.active = 0;
    let data = detailinvoice
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, data, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            resolve(response);
          }
        });
      });
    });
  }

  updateDetailinvoice(detailinvoice: any) {
    let url = API_REFLECT_URL + "update_detailinvoice.php";
    let data = JSON.stringify(detailinvoice);
    let response: any;
    console.log(detailinvoice);
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(url, data, { headers: this.headers })
          .subscribe(data => {
            response = data;
            if (
              typeof response.status !== "undefined" &&
              response.status == false
            ) {
              this.closeSession().then(() => {
                resolve(response);
              });
            } else {
              resolve(response);
            }
          });
      });
    });
  }
}
