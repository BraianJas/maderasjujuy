import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { DetailinvoicesPage } from "./detailinvoices-list/detailinvoices-list.page";

import { ImageCropperModule } from "ngx-image-cropper";

const routes: Routes = [
  {
    path: "",
    component: DetailinvoicesPage
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ImageCropperModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    DetailinvoicesPage,
  ],
  entryComponents: []
})
export class DetailinvoicesModule { }
