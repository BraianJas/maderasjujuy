import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { ProvidersPage } from "./providers-list/providers.page";

import { ProvidersAddComponent } from "./providers-add/providers-add.component";
import { ImageCropperModule } from "ngx-image-cropper";
//import { DriversComponent } from "./users-drivers/drivers/drivers.component";

const routes: Routes = [
  {
    path: "",
    component: ProvidersPage
  },
  {
    path: "add",
    component: ProvidersAddComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ImageCropperModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ProvidersPage,
    ProvidersAddComponent
  ],
  entryComponents: []
})
export class ProvidersPageModule {}
