import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import { FormControl } from "@angular/forms";
import { environment } from "../../../environments/environment";

const API_AUTH_URL = `${environment.API_PATH}auth/`;
const API_REFLECT_URL = `${environment.API_PATH}`;

@Injectable({
  providedIn: "root"
})
export class ProvidersService {
  baseURL: String;
  headers: any;

  constructor(public http: HttpClient, private storage: Storage) {}

  set_headers() {
    return new Promise(resolve => {
      this.storage.get("jwt").then(val => {
        this.headers = new HttpHeaders({
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: "Bearer " + val
        });
        resolve();
      });
      console.log(this.storage);
    });

    
  }

  close: any;
  closeSession() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(API_AUTH_URL + `logout`, {}, { headers: this.headers })
          .subscribe(data => {
            this.storage.remove("maderera-dash-jwt");
            this.close = data;
            resolve(this.close);
          });
      });
    });
  }

  provider: any;
  resultProvider: any;
  profileImg: any;
  getProvider() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "provider", { headers: this.headers })
          .subscribe(data => {
            this.resultProvider = data;
            if (this.resultProvider.length > 0) {
              this.provider = this.resultProvider[0];
              /*this.getProfileImg().then(() => {
                this.user.pic = this.profileImg[0].pic;
                resolve(this.user);
              });*/
              this.provider.pic = "";
              resolve(this.provider);
            } else {
              this.closeSession().then(() => {
                resolve(this.resultProvider);
              });
            }
          });
      });
    });
  }

  // -------------------------- Local DB ------------------------- //
  proveedores: any;
  getProveedores(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        //this.http.get(this.baseURL + `reflect/user/user-group/group?filters=a.active:1,a.last_name LIKE ${param.user}&offset=${param.offset}&limit=${param.limit}`, { headers: this.headers })
        this.http
          .get(API_REFLECT_URL + `proveedores/proveedores-group/group`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.proveedores = data;
            if (
              typeof this.proveedores.status !== "undefined" &&
              this.proveedores.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.proveedores);
              });
            } else {
              resolve(this.proveedores);
            }
          });
      });
    });
  }

  /*groups: any;
  getGroups() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
        .get(`${API_REFLECT_URL}getallgroups.php`,
          {
            headers: this.headers
          }
        )
          .subscribe(data => {
            this.groups = data;
            if (
              typeof this.groups.status !== "undefined" &&
              this.groups.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.groups);
              });
            } else {
              resolve(this.groups);
            }
          });
      });
    });
  }*/

  getProviders() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REFLECT_URL + `proveedor`, { headers: this.headers })
          .subscribe(data => {
            this.proveedores = data;
            if (
              typeof this.proveedores.status !== "undefined" &&
              this.proveedores.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.proveedores);
              });
            } else {
              resolve(this.proveedores);
            }
          });
      });
    });
  }

  //acá hace la llamada al grid de user-list
  /*getProvidersGroup(param) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        //this.http.get(this.baseURL + `reflect/user/user-group/group?filters=a.active:1,first_name last_name dni LIKE ${param.user}&offset=${param.offset}&limit=${param.limit}`, { headers: this.headers })
        this.http
          .get(`${API_REFLECT_URL}getallproviders.php`,
            {
              headers: this.headers
            }
          )
          .subscribe(data => {
            this.proveedores = data;
            if (
              typeof this.proveedores.status !== "undefined" &&
              this.proveedores.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.proveedores);
              });
            } else {
              resolve(this.proveedores);
            }
          });
      });
    });
  }*/

  getAProvider(providerId: number) {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_REFLECT_URL + `provider?filters=a.id:${providerId}`, {
            headers: this.headers
          })
          .subscribe(data => {
            this.provider = data;
            if (
              typeof this.provider.status !== "undefined" &&
              this.provider.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.provider);
              });
            } else {
              resolve(this.provider);
            }
          });
      });
    });
  }

  /*permissions: any;
  getPermission() {
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .get(API_AUTH_URL + "permissions", { headers: this.headers })
          .subscribe(data => {
            this.permissions = data;
            if (
              typeof this.permissions.status !== "undefined" &&
              this.permissions.status == false
            ) {
              this.closeSession().then(() => {
                resolve(this.permissions);
              });
            } else {
              resolve(this.permissions);
            }
          });
      });
    });
  }*/

  addProvider(provider: any): any {

    let url = API_REFLECT_URL + "create_provider.php";

    let dataProvider = JSON.stringify(provider);
    console.log(provider);
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, dataProvider, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            /* let par = {
                "user_id": response.insertId,
                "newPassword": user.password
              };
              this.changeAPassword(par)
                  .subscribe((data) => {
                    console.log(data);
                      if (data.status) {
                          this.user.password = data.password;
                      }
                  }); */
            resolve(response);
          }
        });
      });
    });
  }

  /*   deleteUser(user: any): any {
    let url = this.baseURL + 'reflect/user/';
    let data = '{"id": '+user.id+'}';
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    return this.http.delete(url, httpOptions);
  } */

  deleteProvider(provider: any): any {
    let url = API_REFLECT_URL + "delete_provider.php";
    console.log(provider);
    provider.active = 0;
    let data = provider
    let httpOptions = {
      headers: this.headers,
      body: data
    };
    let response: any;

    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http.post(url, data, { headers: this.headers }).subscribe(data => {
          response = data;
          if (
            typeof response.status !== "undefined" &&
            response.status == false
          ) {
            this.closeSession().then(() => {
              resolve(response);
            });
          } else {
            resolve(response);
          }
        });
      });
    });
  }

  updateProvider(provider: any) {
    let url = API_REFLECT_URL + "update_provider.php";
    let data = JSON.stringify(provider);
    let response: any;
    console.log(provider);
    return new Promise(resolve => {
      this.set_headers().then(() => {
        this.http
          .post(url, data, { headers: this.headers })
          .subscribe(data => {
            response = data;
            if (
              typeof response.status !== "undefined" &&
              response.status == false
            ) {
              this.closeSession().then(() => {
                resolve(response);
              });
            } else {
              resolve(response);
            }
          });
      });
    });
  }
}

