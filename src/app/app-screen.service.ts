import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ScreenService {

  screen = new BehaviorSubject({ 
    sm: false, 
    md: false, 
    lg: false, 
    xl: false, 
    width: 0, 
    height: 0 
  });

  constructor(
    private plt: Platform
  ) 
  {
    this.plt.ready().then(() => {
      this.onResize();
    });
  }

  onResize() {
    let sm = false;
    let md = false;
    let lg = false;
    let xl = false;

    let width = window.innerWidth;
    let height = window.innerHeight;
    if (width <= 576) {
      sm = true;
    }
    if (width > 576 && width <= 768) {
      md = true;
    }
    if (width > 768 && width <= 992) {
      lg = true;
    }
    if (width > 992) {
      xl = true;
    }

    this.screen.next({ sm, md, lg, xl, width, height });
  }
}
